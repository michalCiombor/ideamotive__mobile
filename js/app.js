document.addEventListener("DOMContentLoaded", () => {

    //slider controlls

    const leftButton = document.querySelector(".leftArrow");
    const rightButton = document.querySelector(".rightArrow");
    const slider = document.querySelector(".slider__slides");
    const nav = document.querySelector(".mainNav__wrapper");
    const burger = document.querySelector(".mainNav__burger");




    //menu mobile
    burger.addEventListener("click",()=>{
        nav.classList.toggle('active');
    })

    //slider controlls
    let n = 0;

    leftButton.addEventListener("click", () => {
        if (n <= -1) {
            n = 0;
        } else {
            n--;
            slider.style.setProperty("transform", `translateX(${-n * 100}vw)`);
        }
    });
    rightButton.addEventListener("click", () => {
        if (n > 1) {
            n = 1;
        } else {
            slider.style.setProperty("transform", `translateX(${-n * 100}vw)`);
            n++;
        }
    })

    // pictures for slider JS
    const slideImgs = document.querySelectorAll(".slide>img");
    function changeSrc(variable) {
        const oldSrc = variable.getAttribute('src');
        console.log(oldSrc);
        let newSrc = oldSrc.split("@").slice(0,1);
        newSrc.push("2x.jpg");
        newSrc = newSrc.join("@");
        variable.setAttribute('src',newSrc);
    }
    if (window.innerWidth > 768) {
        for (img of slideImgs) {
            changeSrc(img);
        }
    }

});
window.addEventListener("resize",()=>{
    console.log("aby przeładować zdjęcia slidera wciśnij f5")
})